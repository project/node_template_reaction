<?php

/**
 * Node template as context reactions.
 */
class context_reaction_node_template extends context_reaction {
  /**
   * Editor form.
   */
  function editor_form($context) {
    $form = $this->options_form($context);

    return $form;
  }

  /**
   * Submit handler for editor form.
   */
  function editor_form_submit($context, $values) {
    return $values;
  }

  /**
   * Allow admins to set node template.
   */
  function options_form($context) {
    $values = $this->fetch_from_context($context);
    $form = array(
      'name' => array(
        '#title' => t('Template name'),
        '#description' => t('If template file has name node--my-template.tpl.php template name must be my-template.'),
        '#type' => 'textfield',
        '#maxlength' => 255,
        '#default_value' => isset($values['name']) ? $values['name'] : '',
      ),
    );
    return $form;
  }

  /**
   * Set node template.
   */
  function execute(&$vars) {
    foreach ($this->get_contexts() as $value) {
      if (empty($value->reactions['node_template']['name'])) {
        return;
      }
      $theme_hook_suggestion = 'node__' . str_replace('-', '_', $value->reactions['node_template']['name']);

      if (isset($value->conditions['node']['values'])) {
        foreach ($value->conditions['node']['values'] as $type) {
          if ($type == $vars['node']->type) {
            $vars['theme_hook_suggestions'][] = $theme_hook_suggestion;
            break;
          }
        }
      }
      if (isset($value->conditions['path']['values'])) {
        foreach ($value->conditions['path']['values'] as $path) {
          if ($path == 'node/' . $vars['node']->nid) {
            $vars['theme_hook_suggestions'][] = $theme_hook_suggestion;
            break;
          }
          elseif ($path == '~node/' . $vars['node']->nid) {
            // Remove suggestion.
            foreach ($vars['theme_hook_suggestions'] as $key => $item) {
              if ($item == $theme_hook_suggestion) {
                unset($vars['theme_hook_suggestions'][$key]);
              }
            }
          }
        }
      }
    }
  }
}
